- Tout d’abord il faut activer l’url rewriting.
Ensuite il serait préférable de la placer à la racine du fait des lien absolue  du au rewriting des urls. Vous pouvez créer un utilisateur “test_user” sans mot de passe avant d’importer la base de donnée webreathe se trouvant dans le fichier webreathe.sql ou directement modifier le fichier module.php se trouvant dans le dossier model.

- Pour la navigation:
Une fois sur la page d’accueil, on a un aperçu sur l’ensemble des modules déjà créer, on peut voir les notifications, un menu qui nous fait retourner sur la page d'accueil et enfin un bouton ajouter module qui nous permet d’ajouter de nouveau module.

En dehors de la page d'accueil, il existe deux autres pages:
détails : Dans lequel on trouve les options des modules (état de marche, …).
forms: Qui nous permet de créer un nouveau module..

- Sécurité:
Sur le plan de la sécurité, elle n’est absolument pas énorme, juste quelque empty et isset sur certains données et la présence du rewriting.


- Estimant qu’en principe les données d’un module iot doivent être transmis par la le module elle même, ainsi au niveau du formulaire, quand on coche une case, il y’a une valeur par défaut qui est choisi afin d’alimenter la base de donnée.


- Utilisation du template: CoolAdmin Bootstrap 4.1 Admin Dashboard Template de https://colorlib.com/polygon/cooladmin/index.html

