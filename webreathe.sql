-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 20 avr. 2020 à 19:06
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `webreathe`
--

-- --------------------------------------------------------

--
-- Structure de la table `iot_module`
--

CREATE TABLE `iot_module` (
  `id_module` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `batterie` int(11) DEFAULT NULL,
  `donnee` int(11) DEFAULT NULL,
  `etat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `iot_module`
--

INSERT INTO `iot_module` (`id_module`, `numero`, `nom`, `description`, `type`, `temperature`, `batterie`, `donnee`, `etat`) VALUES
(1, 4, 'Montre', 'montre de nicky larson', NULL, 37, NULL, 15, NULL),
(2, 4, 'nom', 'Description', 'type', 37, NULL, 15, 1),
(3, 4, 'nom', 'Description', 'type', 37, NULL, 15, 0),
(4, 14, 'Montre', 'montre de laura marconi', 'xyz', NULL, 50, NULL, 1),
(5, 78, 'Financement', 'ilrelililblbii', 'bip', NULL, NULL, NULL, 1),
(6, 78, 'uizruirzu', 'urizuizriurzui', 'rzuiuireui', 37, 50, NULL, NULL),
(7, 78, 'uizruirzu', 'urizuizriurzui', 'rzuiuireui', 37, 50, NULL, NULL),
(8, 78, 'uizruirzu', 'urizuizriurzui', 'rzuiuireui', 37, 50, NULL, NULL),
(9, 78, 'uizruirzu', 'urizuizriurzui', 'rzuiuireui', 37, 50, NULL, NULL),
(10, 78, 'uizruirzu', 'urizuizriurzui', 'rzuiuireui', 37, 50, NULL, NULL),
(11, 78, 'uizruirzu', 'urizuizriurzui', 'rzuiuireui', 37, 50, NULL, NULL),
(12, 4, 'jkjk', 'jkhkjkjk', 'jkhjkkhjk', NULL, NULL, NULL, 1),
(13, 4, 'jkjk', 'jkhkjkjk', 'jkhjkkhjk', NULL, NULL, NULL, 1),
(14, 4, 'test', 'hethrh', 'ghfilhgi', NULL, NULL, NULL, 1),
(15, 4, 'rere', 'erer', 'er', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

CREATE TABLE `notification` (
  `id_notification` int(11) NOT NULL,
  `details` varchar(255) NOT NULL,
  `date_notification` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `notification`
--

INSERT INTO `notification` (`id_notification`, `details`, `date_notification`) VALUES
(1, 'creation du module', '2000-04-20 01:59:32'),
(2, 'creation du modulejkjk', '2020-04-20 02:14:39'),
(3, 'creation du module test', '2020-04-20 15:28:56'),
(4, 'creation du module rere', '2020-04-20 18:28:12');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `iot_module`
--
ALTER TABLE `iot_module`
  ADD PRIMARY KEY (`id_module`);

--
-- Index pour la table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id_notification`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `iot_module`
--
ALTER TABLE `iot_module`
  MODIFY `id_module` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `notification`
--
ALTER TABLE `notification`
  MODIFY `id_notification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
