<?php
/**
 * classe module 
 */
class Module
{
	/**
	* Connexion à la base de donnée webreathe
	*/
	private function getBdd() 
	{
		$db = new PDO('mysql:host=localhost;dbname=webreathe;charset=utf8', 'test_user', '',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
		return $db;
	}

	/**
	* methode getModule() qui permet de récuperer les modules
	*/

	public function getModule()
	{
		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		$req = $db->prepare('SELECT id_module, nom, numero, description, type FROM iot_module');
		$req->execute();
		$module = $req->fetchAll();

		return $module;
	}

	/**
	* methode getNotification() qui permet de récuperer les Notifications
	*/

	public function getAllNotification()
	{
		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		$req = $db->prepare('SELECT details, date_notification FROM notification');
		$req->execute();
		$notification = $req->fetchAll();

		return $notification;
	}

	/**
	* methode getAllNotification() qui permet de récuperer les Notifications
	*/

	public function getCountNotification()
	{
		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		$req = $db->prepare('SELECT count(id_notification) as nombre FROM notification');
		$req->execute();
		$notification = $req->fetchAll();

		return $notification;
	}

	/**
	* methode getDetails() qui permet de récupérer les options d'un module
	* @param id_module : correspond à l'identifiant d'un module 
	*/

	public function getDetails($id_module)
	{
		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}

		$req = $db->prepare('SELECT nom, description, batterie, temperature, etat, donnee FROM iot_module WHERE id_module = ?');
		$req->execute(array($id_module));

		$module = $req->fetchAll();

		return $module;
	}

	/**
	* methode setModule() qui permet d'insérer les options d'un module
	* @param $le_post: l'ensemble des données à insérer,
	*/

	public function setModule($le_post)
	{
		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		$tab = 'INSERT INTO iot_module(';
		foreach( $le_post as $index=>$valeur){
			$tab .= $index.',';

			if (strcmp($index,"nom") == 0) {
				$this->setNotification($valeur);
			}
		}
		$tab = substr($tab,0,-1);
		$tab .= ') VALUES (';

		foreach( $le_post as $index=>$valeur){
			$tab .= '"'.$valeur.'",';

			
		}
		$tab = substr($tab,0,-1);
		$tab .= ')';

		$req = $db->prepare($tab);
		$req->execute();
		
		$req = $db->prepare('SELECT id_module FROM module ORDER BY id_module DESC LIMIT 1');
		$req->execute();
		$module = $req->fetchAll();

		return $module;
	}

	/**
	* methode setNotification() qui permet d'insérer les options d'un Notification
	* @param $details: l'ensemble des données à insérer,
	*/

	public function setNotification($details)
	{
		$details = "creation du module ".$details;

		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}

		$tab = 'INSERT INTO notification(details, date_notification) values ('.'"'.$details.'","'.date("Y-m-d H:i:s").'")';

		$req = $db->prepare($tab);
		$req->execute();
		
		return $req;
	}

}

?>