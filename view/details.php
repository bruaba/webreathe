<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <!-- Title Page-->
    <title>Details Modules</title>
    <link rel="icon" type="image/png" href="/webreathe/view/images/icon/logo-whitee.png" />

    <!-- Fontfaces CSS-->
    <link href="../view/css/font-face.css" rel="stylesheet" media="all">
    <link href="../view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../view/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../view/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../view/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">

        <!-- MENU SIDEBAR-->
        <?php require('../view/menu.php'); ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php require('../view/header.php'); ?>
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">

                                    <div class="au-card-title" style="background-image:url('images/bg-title-01.jpg');">
                                        <div class="bg-overlay bg-overlay--blue"></div>
                                        <h3>
                                            <?php
                                            foreach ($donnees as $donnee) 
                                            {
                                                ?>
                                                <i class="zmdi zmdi-account-calendar"></i><?php echo $donnee['nom']; ?> 

                                                <?php
                                            }
                                            ?>
                                        </h3>
                                    </div>

                                    <div class="au-task js-list-load">
                                        <?php
                                        foreach ($donnees as $donnee) 
                                        {
                                            ?>
                                            <div class="au-task__title">
                                                <p><?php echo $donnee['description']; ?></p>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="au-task-list js-scrollbar3">

                                            <?php

                                            foreach ($donnees as $nom => $donnee) 
                                            {

                                                foreach($donnee as $d => $o)
                                                {


                                                    if ((strcmp($d,"batterie") == 0 ) && (!empty($donnee['batterie']))) 
                                                    {

                                                        ?>
                                                        <div class="au-task__item au-task__item--danger">
                                                            <div class="au-task__item-inner">
                                                                <h5 class="task">
                                                                    <a href="#"><?php echo $d; ?></a>
                                                                </h5>

                                                                <div class="progress mb-2">
                                                                    <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $donnee['batterie']; ?>%" aria-valuenow="<?php echo $donnee['batterie']; ?>" aria-valuemin="0" aria-valuemax="100"><?php echo $donnee['batterie']; ?>%</div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <?php
                                                    }

                                                }


                                                if (!empty($donnee['temperature'])) {

                                                    ?>
                                                    <div class="au-task__item au-task__item--success">
                                                        <div class="au-task__item-inner">
                                                            <h5 class="task">
                                                                <a href="#">Température : <?php echo $donnee['temperature']; ?>‎</a>
                                                            </h5>


                                                        </div>
                                                    </div>

                                                    <?php
                                                }

                                                if (!empty($donnee['donnee'])) {

                                                    ?>
                                                    <div class="au-task__item au-task__item--success">
                                                        <div class="au-task__item-inner">
                                                            <h5 class="task">
                                                                <a href="#">Données envoyées : <?php echo $donnee['donnee']; ?>‎</a>
                                                            </h5>


                                                        </div>
                                                    </div>

                                                    <?php
                                                }


                                                if (!empty($donnee['etat'])) {

                                                    ?>
                                                    <div class="au-task__item au-task__item--primary">
                                                        <div class="au-task__item-inner">
                                                            <h5 class="task">
                                                                <a href="#">Etat de Marche</a>
                                                            </h5>
                                                            <?php
                                                            if ($donnee['etat']) {

                                                                ?>
                                                                <button type="button" class="btn btn-success">Allumée</button>
                                                                <?php
                                                            }
                                                            else{
                                                                ?>
                                                                <button type="button" class="btn btn-danger">Eteint</button>

                                                                <?php
                                                            }

                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div> 
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTAINER-->

    </div>

    <!-- Jquery JS-->
    <script src="../view/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../view/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../view/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../view/vendor/slick/slick.min.js">
    </script>
    <script src="../view/vendor/wow/wow.min.js"></script>
    <script src="../view/vendor/animsition/animsition.min.js"></script>
    <script src="../view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../view/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../view/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../view/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../view/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../view/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../view/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../view/js/main.js"></script>

</body>

</html>
<!-- end document-->
