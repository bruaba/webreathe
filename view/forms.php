<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Forms</title>
    <link rel="icon" type="image/png" href="/webreathe/view/images/icon/logo-whitee.png" />

    <!-- Fontfaces CSS-->
    <link href="/webreathe/view/css/font-face.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="/webreathe/view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="/webreathe/view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/webreathe/view/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <?php require('../view/menu.php'); ?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <?php require('../view/header.php'); ?>

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Ajouter</strong> un module
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nom</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="nom" name="nom" placeholder="nom du module" class="form-control">
                                                    <small class="form-text text-muted">nom du module</small>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="textarea-input" class=" form-control-label">Description</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <textarea name="description" id="description" rows="9" placeholder="Description..." class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Type</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="type" name="type" placeholder="Type" class="form-control">
                                                    <small class="form-text text-muted">Type</small>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Numéro du module</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="numero" name="numero" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label">Options</label>
                                                </div>
                                                <div class="col col-md-9">
                                                    <div class="form-check">
                                                        <div class="checkbox">
                                                            <label for="checkbox1" class="form-check-label ">
                                                                <input type="checkbox" id="temperature" name="temperature" value="37" class="form-check-input">Temperature
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label for="checkbox2" class="form-check-label ">
                                                                <input type="checkbox" id="batterie" name="batterie" value="50" class="form-check-input"> Durée de fonctionnement
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label for="checkbox3" class="form-check-label ">
                                                                <input type="checkbox" id="donnee" name="donnee" value="15" class="form-check-input"> Nombre de données envoyées
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label for="checkbox3" class="form-check-label ">
                                                                <input type="checkbox" id="etat" name="etat" value="1" class="form-check-input"> Etat de marche
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-dot-circle-o"></i> Enregistrer
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="/webreathe/view/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="/webreathe/view/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="/webreathe/view/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="/webreathe/view/vendor/slick/slick.min.js">
    </script>
    <script src="/webreathe/view/vendor/wow/wow.min.js"></script>
    <script src="/webreathe/view/vendor/animsition/animsition.min.js"></script>
    <script src="/webreathe/view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="/webreathe/view/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="/webreathe/view/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="/webreathe/view/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="/webreathe/view/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/webreathe/view/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="/webreathe/view/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="/webreathe/view/js/main.js"></script>

</body>

</html>
<!-- end document-->
