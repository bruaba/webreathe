<!-- HEADER DESKTOP-->
<header class="header-desktop">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="header-wrap">
                <div class="noti__item js-item-menu">
                    <i class="zmdi zmdi-notifications"></i>
                    <?php
                    foreach ($countHeader as $donnee) 
                    {
                        ?>
                        <span class="quantity"><?php echo $donnee['nombre'] ?></span>
                        <?php
                    }
                    ?>
                    <div class="notifi-dropdown js-dropdown">
                        <?php
                        foreach ($countHeader as $donnee) 
                        {
                            ?>
                            <div class="notifi__title">
                                <p>You have <?php echo $donnee['nombre'] ?> Notifications</p>
                            </div>
                            <?php
                        }
                        foreach ($donneesHeader as $donnee) 
                        {
                            ?>

                            <div class="notifi__item">
                                <div class="bg-c1 img-cir img-40">
                                    <i class="zmdi zmdi-email-open"></i>
                                </div>
                                <div class="content">
                                    <p><?php echo $donnee['details'] ?></p>
                                    <span class="date"><?php echo $donnee['date_notification'] ?></span>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="notifi__footer">
                            <a href="#">All notifications</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</header>
<!-- HEADER DESKTOP-->
