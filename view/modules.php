<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Modules</title>
    <link rel="icon" type="image/png" href="/webreathe/view/images/icon/logo-whitee.png" />

    <!-- Fontfaces CSS-->
    <link href="/webreathe/view/css/font-face.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="/webreathe/view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="/webreathe/view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/webreathe/view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/webreathe/view/css/theme.css" rel="stylesheet" media="all">

</head>



<body class="animsition">
    <div class="page-wrapper">

        <?php require('../view/menu.php'); ?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <?php require('../view/header.php'); ?>

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Modules</h2>
                                    <a href="form"><button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>ajouter un module</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-25">
                            <?php
                            foreach ($donnees as $donnee) 
                            {
                        ?>
                        
                        <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c<?php echo random_int(1, 4);?>">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            
                                            <div class="text">
                                                <h2><?php echo $donnee['nom']; ?></h2>
                                                <p><?php echo $donnee['numero']; ?></p>
                                                <span><?php echo $donnee['description']; ?></span>
                                            </div>
                                            <div class="au-task__footer">
                                                <a href="detail/<?php echo $donnee['id_module'];?>"><button class="btn btn-outline-warning">en savoir +</button></a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © <?php echo date("Y"); ?> Bruaba. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="/webreathe/view/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="/webreathe/view/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="/webreathe/view/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="/webreathe/view/vendor/slick/slick.min.js">
    </script>
    <script src="/webreathe/view/vendor/wow/wow.min.js"></script>
    <script src="/webreathe/view/vendor/animsition/animsition.min.js"></script>
    <script src="/webreathe/view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="/webreathe/view/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="/webreathe/view/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="/webreathe/view/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="/webreathe/view/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/webreathe/view/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="/webreathe/view/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="/webreathe/view/js/main.js"></script>

</body>

</html>
<!-- end document-->
