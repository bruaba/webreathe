<?php

// Chargement des classes
require_once('../model/module.php');

$model = new Module();

$donneesHeader = $model->getAllNotification();
$countHeader = $model->getCountNotification();
$donnees = $model->getModule();

foreach($donnees as $cle => $donnee)
{
    $donnees[$cle]['nom'] = htmlspecialchars($donnee['nom']);
    $donnees[$cle]['numero'] = htmlspecialchars($donnee['numero']);
    $donnees[$cle]['description'] = nl2br(htmlspecialchars($donnee['description']));
    $donnees[$cle]['type'] = nl2br(htmlspecialchars($donnee['type']));
}

include_once('../view/modules.php');

?>