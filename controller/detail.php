<?php

// Chargement des classes
require_once('../model/module.php');

$model = new Module();

$donneesHeader = $model->getAllNotification();
$countHeader = $model->getCountNotification();


$donnees = $model->getDetails($_GET["id_module"]);

foreach($donnees as $cle => $donnee)
{
	$donnees[$cle]['temperature'] = htmlspecialchars($donnee['temperature']);
	$donnees[$cle]['batterie'] = nl2br(htmlspecialchars($donnee['batterie']));
	$donnees[$cle]['donnee'] = nl2br(htmlspecialchars($donnee['donnee']));
	$donnees[$cle]['etat'] = nl2br(htmlspecialchars($donnee['etat']));
	$donnees[$cle]['nom'] = nl2br(htmlspecialchars($donnee['nom']));
	$donnees[$cle]['description'] = nl2br(htmlspecialchars($donnee['description']));
}


include_once('../view/details.php');

?>